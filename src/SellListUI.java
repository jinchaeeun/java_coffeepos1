import java.awt.Container;
import java.awt.Panel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import bean.Coffee;
import dao.coffeedao;
import javax.swing.ScrollPaneConstants;
import javax.swing.JLabel;
import java.awt.Font;
//판매보기
public class SellListUI extends JFrame{
	Container container = getContentPane();
	Panel pal =new Panel();
	coffeedao dao=null;

	Vector<Coffee> rowData = null; //벡터를 써야 배열이 무한대로 생성된다고 함. 
	Object columnNames[] = {"메뉴","사이즈","샷","HOT/ICE","PRICE"};
	Object sellNames[] = {"메뉴","판매잔수"};
	JLabel lblNewLabel_1;
	private JTable table;
	private JTable table_1;

	public SellListUI() {
		dao = new coffeedao();	
		rowData =dao.GetAllSellList();
		//창 크기
		setSize(550, 700);
		setVisible(true);
		setLocation(400, 100);
		getContentPane().setLayout(null);
		
		
		JPanel panel = new JPanel();
		panel.setBounds(25, 41, 500, 215);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		//db
		table = new JTable(dao.makeArr(dao.GetAllSellList()),columnNames);
		table.setBounds(1, 27, 450, 288);
		panel.add(table);
		//스윙, 스크롤
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(13, 5, 469, 200);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		panel.add(scrollPane);
			
		//메뉴별판매량
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(25, 300, 500, 215);
		getContentPane().add(panel_3);
		panel_3.setLayout(null);
		
		table_1 = new JTable(dao.makelistArr(dao.Getsellcount()),sellNames);
		table_1.setBounds(26, 0, 450, 215);
		panel_3.add(table_1);
		//스크롤
		JScrollPane scrollPane_1 = new JScrollPane(table_1);
		scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_1.setBounds(12, 0,  469, 215);
		
		panel_3.add(scrollPane_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 577, 534, 75);
		getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JButton button = new JButton("총 매출");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {//테이블에 전체 매출 리스트 출력
				rowData =dao.GetAllSellList();
				int sum=0;
				for(int i=0; i<rowData.size();i++){					
					sum += rowData.get(i).getPrice();
				}
				lblNewLabel_1.setText("총 매출액은 "+Integer.toString(sum)+"원 입니다!");
			}
		});
		button.setBounds(152, 10, 100, 50);
		panel_1.add(button);
		
		JButton btnNewButton = new JButton("창 닫기");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {//창닫기
				setVisible(false);
			}
		});
		
		btnNewButton.setBounds(285, 10, 100, 50);
		panel_1.add(btnNewButton);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(100, 524, 534, 43);
		getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setFont(new Font("굴림", Font.PLAIN, 25));
		lblNewLabel_1.setBounds(40, 0, 400, 40);
		panel_2.add(lblNewLabel_1);
	}
}
