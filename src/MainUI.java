import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

import bean.Coffee;
import dao.coffeedao;

//입력은 여기서
public class MainUI extends JFrame{	
	Vector<Coffee> list =new Vector<Coffee>();
	Coffee coffee = null; 
	coffeedao dao = null;
	JLabel lblNewLabel_1;
	String menu="";
	boolean flag_size=false;//한번이라도 클릭하면 true
	boolean flag_shot=false;
	private JFrame frame;
	
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainUI window = new MainUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public MainUI() {
		initialize();
	}
	
	public void initialize() {
		coffee = new Coffee();
		dao = new coffeedao();
		frame = new JFrame();
		frame.setBounds(400, 100, 600, 740); //메인 창 크기
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //창 완전히 닫기
		frame.getContentPane().setLayout(null); //getContentPane() 패널과 상속받은 모든 컴포넌트 삽입 가능 기본적으로 BorderLayout이라 null로 줌
		frame.setTitle("smile coffee");
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(230, 162, 150)); //메뉴 창 색깔
		panel.setBounds(0, 0, 700, 450);  //메뉴 버튼 있는 창 크기 
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		
		//버튼 선언
		
		
		JButton button_0 = new JButton();
		button_0.setBounds(0, 0, 200, 150);
		panel.add(button_0);
		
		JButton button_1 = new JButton();
		button_1.setBounds(200, 0, 200, 150);
		panel.add(button_1);
		
		JButton button_2 = new JButton();
		button_2.setBounds(400, 0, 200, 150);
		panel.add(button_2);
		
		JButton button_3 = new JButton();
		button_3.setBounds(0, 150, 200, 150);
		panel.add(button_3);
		
		JButton button_4 = new JButton();
		button_4.setBounds(200, 150, 200, 150);
		panel.add(button_4);
		
		JButton button_5 = new JButton();
		button_5.setBounds(400, 150, 200, 150);
		panel.add(button_5);
		
		JButton button_6 = new JButton();
		button_6.setBounds(0, 300, 200, 150);
		panel.add(button_6);
		
		JButton button_7 = new JButton();
		button_7.setBounds(200, 300, 200, 150);
		panel.add(button_7);
		
		JButton button_8 = new JButton();
		button_8.setBounds(400, 300, 200, 150);
		panel.add(button_8);
		//버튼 배경 색
		
		int r=209;
		int g=202;
		int b=202;
		Color color=new Color(r,g,b);
		button_0.setBackground(color);
		button_1.setBackground(color);
		button_2.setBackground(color);
		button_3.setBackground(color);
		button_4.setBackground(color);
		button_5.setBackground(color);
		button_6.setBackground(color);
		button_7.setBackground(color);
		button_8.setBackground(color);
		
		//버튼 이미지
		button_0.setIcon(new ImageIcon("C:/Users/kbg05/eclipse-workspace/coffeePos222/에스프레소.png"));
		button_1.setIcon(new ImageIcon("C:/Users/kbg05/eclipse-workspace/coffeePos222/아메리카노.png"));
		button_2.setIcon(new ImageIcon("C:/Users/kbg05/eclipse-workspace/coffeePos222/카페라떼.png"));
		button_3.setIcon(new ImageIcon("C:/Users/kbg05/eclipse-workspace/coffeePos222/카라멜마끼아또.png"));
		button_4.setIcon(new ImageIcon("C:/Users/kbg05/eclipse-workspace/coffeePos222/레몬에이드.png"));
		button_5.setIcon(new ImageIcon("C:/Users/kbg05/eclipse-workspace/coffeePos222/청포도에이드.png"));
		button_6.setIcon(new ImageIcon("C:/Users/kbg05/eclipse-workspace/coffeePos222/녹차라떼.png"));
		button_7.setIcon(new ImageIcon("C:/Users/kbg05/eclipse-workspace/coffeePos222/초코라떼.png"));
		button_8.setIcon(new ImageIcon("C:/Users/kbg05/eclipse-workspace/coffeePos222/고구마라떼.png"));
		
		
		//이벤트
		button_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { showlabel("에스프레소",2500); } });
		
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { showlabel("아메리카노",2800); } });
		
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { showlabel("카페라떼",3200); } });
		
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { showlabel("카라멜마끼아또",3500); } });
		
		button_4.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {showlabel("레몬에이드",3800); } });
		
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { showlabel("청포도에이드",3800); } });
		
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { showlabel("녹차라떼",3200); } });
		
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { showlabel("초코라떼",3200); } });
		
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { showlabel("고구마라떼",4000); } });

		//버튼
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 416, 784, 155);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("  ICE");
		buttonGroup.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				coffee.setTemp("ICE");
				showlabel(coffee.getName(),coffee.getPrice());
				
				
			}
		});
		rdbtnNewRadioButton.setBounds(145, 56, 60, 26);
		panel_1.add(rdbtnNewRadioButton);
		JRadioButton rdbtnHot = new JRadioButton("  HOT");
		buttonGroup.add(rdbtnHot);
		rdbtnHot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				coffee.setTemp("HOT");
				showlabel(coffee.getName(),coffee.getPrice());
			}
		});
		rdbtnHot.setBounds(209, 56, 121, 26);
		panel_1.add(rdbtnHot);
		
		JRadioButton rdbtnSmall = new JRadioButton("  SMALL");
		buttonGroup_1.add(rdbtnSmall);
		rdbtnSmall.addActionListener(new ActionListener() {
				
			public void actionPerformed(ActionEvent e) {			
				clicksize(coffee.getSize(),"SMALL",flag_size);
				coffee.setSize("SMALL");
				showlabel(coffee.getName(),coffee.getPrice());
				flag_size=true;
			}
		});
		rdbtnSmall.setBounds(145, 87, 80, 26);
		panel_1.add(rdbtnSmall);
		
		JRadioButton rdbtnTall = new JRadioButton("  TALL");
		buttonGroup_1.add(rdbtnTall);
		rdbtnTall.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				clicksize(coffee.getSize(),"TALL",flag_size);
				coffee.setSize("TALL");
				showlabel(coffee.getName(),coffee.getPrice());
				flag_size=true;
			}
		});
		rdbtnTall.setBounds(226, 87, 80, 26);
		panel_1.add(rdbtnTall);
		
		JRadioButton rdbtnLarge = new JRadioButton("  LARGE");
		buttonGroup_1.add(rdbtnLarge);
		rdbtnLarge.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				clicksize(coffee.getSize(),"LARGE",flag_size);
				coffee.setSize("LARGE");
				showlabel(coffee.getName(),coffee.getPrice());
				flag_size=true;
				
			}
		});
		rdbtnLarge.setBounds(307, 87, 80, 26);
		panel_1.add(rdbtnLarge);
		
		JLabel lblNewLabel = new JLabel("HOT / ICE");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 15));
		lblNewLabel.setBounds(59, 60, 99, 18);
		panel_1.add(lblNewLabel);
		
		JLabel label_1 = new JLabel("샷추가");
		label_1.setFont(new Font("Dialog", Font.BOLD, 15));
		label_1.setBounds(59, 120, 99, 18);
		panel_1.add(label_1);
		
		JLabel label = new JLabel("음료 사이즈");
		label.setFont(new Font("Dialog", Font.BOLD, 15));
		label.setBounds(59, 90, 99, 18);
		panel_1.add(label);
		
		JRadioButton rdbtnYes = new JRadioButton("  YES");
		buttonGroup_2.add(rdbtnYes);
		rdbtnYes.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				clickshot(coffee.getShot(), "샷추가", flag_shot);
				coffee.setShot("샷추가");
				showlabel(coffee.getName(),coffee.getPrice());
				flag_shot= true;
		
				
			}
		});
		rdbtnYes.setBounds(145, 117, 60, 26);
		panel_1.add(rdbtnYes);
		
		JRadioButton rdbtnNo = new JRadioButton("  NO");
		buttonGroup_2.add(rdbtnNo);
		rdbtnNo.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if(coffee.getShot()==("샷추가")){
					coffee.setPrice(coffee.getPrice()-500);
				}
				
				coffee.setShot("NO샷");
				showlabel(coffee.getName(),coffee.getPrice());
			}
		});
		rdbtnNo.setBounds(209, 117, 121, 26);
		panel_1.add(rdbtnNo);
		//라디오버튼
		
//___________________________________________________


		
		
//--------------------------------------------------------
		//메인 맨 아래 버튼
	
		JPanel panel_2 = new JPanel();// {

		panel_2.setBounds(0, 462, 784, 217);
		frame.getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		lblNewLabel_1 = new JLabel();
		lblNewLabel_1.setFont(new Font("굴림", Font.BOLD, 20));
		lblNewLabel_1.setText("");
		
		lblNewLabel_1.setBounds(81, 120, 618, 26); //주문내역확인창
		panel_2.add(lblNewLabel_1);
		lblNewLabel_1.setText(menu);		
		
		JButton bt_2 = new JButton("결제");//결제 누르면 서버로 입력
		bt_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {							
					dao.coffeeadd(coffee);//판매정보 데이터에 저장
					System.out.println(coffee);
					//재고줄어들게

					//버튼초기화
					buttonGroup.clearSelection();
					buttonGroup_1.clearSelection();  //사이즈
					buttonGroup_2.clearSelection();
					lblNewLabel_1.setText("");
					JOptionPane.showMessageDialog(null, "결제되었습니다");
					//재고가 자동관리 메소드
					coffee.setName(null);
					coffee.setShot(null);
					coffee.setSize(null);
					coffee.setTemp(null);
					coffee.setPrice(0);
					flag_size=false;
					flag_shot=false;
//				}

			
			}
		});
		bt_2.setBounds(238, 171, 123, 38);
		panel_2.add(bt_2);
		
		JButton bt_3 = new JButton("취소");
		bt_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				flag_shot=false;
				flag_size=false;
				coffee.setName(null);
				coffee.setShot(null);
				coffee.setSize(null);
				coffee.setTemp(null);
				coffee.setPrice(0);
				lblNewLabel_1.setText("");
				// 버튼 초기화
				buttonGroup.clearSelection();
				buttonGroup_1.clearSelection();
				buttonGroup_2.clearSelection(); 
				System.out.println(coffee);
				JOptionPane.showMessageDialog(null, "선택 취소되었습니다.");
				
			}
		});
		bt_3.setBounds(431, 171, 123, 38);
		panel_2.add(bt_3);
		
		JButton bt_1 = new JButton("판매보기"); //판매확인
		bt_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new SellListUI();
				System.out.println(dao.GetAllSellList());								
			}
		});
		bt_1.setBounds(57, 171, 123, 38);
		panel_2.add(bt_1);
	}

	//옵션 버튼 눌렀을 때 확인할 수 있게 레이블로 띄워줌 Coffee.java에서 리턴해옴
	
	public void showlabel(String name, int price){//판매 데이터 입력
		
		coffee.setName(name);
		coffee.setPrice(price);
				
		if(coffee.getName() != null && coffee.getPrice() != 0 ){
			menu = coffee.getName()+ " / " + coffee.getPrice();
			
			if(coffee.getShot() != null){
				menu = menu+ " / " +coffee.getShot();
			}
			
			if(coffee.getSize() != null){
				menu = menu+ " / " +coffee.getSize();
			}
			
			if(coffee.getTemp() != null){
				menu = menu+ " / " +coffee.getTemp();
			}
		}		
		lblNewLabel_1.setText(menu);
		
		
		
	}//showlabel


/*##사이즈클릭설정##*/
	public void clicksize(String befor_size,String size,boolean flag_size){// 사이즈관련된 클릭정보관리 메소드
		
		if(flag_size){
			//두번째 이상
			switch (size) {
				case "SMALL":	//스몰을 눌렀을 때
					switch (befor_size) {  //그 전 사이즈가
						case "TALL":	//톨이면 -500원
							coffee.setPrice(coffee.getPrice()-500);
							break;
						case "LARGE":	//라지였으면 -1000원
							coffee.setPrice(coffee.getPrice()-1000);
							break;
						default: /*small이었다면 그대로*/ break;
					}				
					break;
					
					case "TALL":
						switch (befor_size) {
						case "SMALL":
							coffee.setPrice(coffee.getPrice()+500);
							break;
						case "LARGE":
							coffee.setPrice(coffee.getPrice()-500);
							break;
						default: break;
						}
					break;
					
					case "LARGE":
						switch (befor_size) {
							case "SMALL":
								coffee.setPrice(coffee.getPrice()+1000);
								break;
							case "TALL":
								coffee.setPrice(coffee.getPrice()+500);
								break;
							default: break;
						}
					break;
					
					default: break;
			}
		}
		else{ //한번 눌렀을 때는
			switch (size) {
				case "TALL":
					coffee.setPrice(coffee.getPrice()+500);
					break;
				case "LARGE":
					coffee.setPrice(coffee.getPrice()+1000);
					break;	
				default: break;
			}
		}
	}
	
	/*##샷클릭설정##*/
	public void clickshot(String befor_shot,String shot,boolean flag_shot){//샷관련된 버튼관리
		if(flag_shot){
			//두번이상
			switch (shot){
			case "샷추가":
				switch (befor_shot) {
				case "샷추가":
					break;
				case "NO샷":
					coffee.setPrice(coffee.getPrice()+500);
					break;		
				default: break;
				}
				break;
				
			case "NO샷":
				switch (befor_shot) {
				case "샷추가":
					coffee.setPrice(coffee.getPrice()-500);
					break;
				case "NO샷":
					break;		
				default: break;
				}
				break;		
			default: break;
			}
		}
		else {
			//한번 누를 때
			switch (shot) {
				case "샷추가": coffee.setPrice(coffee.getPrice()+500); break;
				case "NO샷":  break;		
				default: break;
			}
		}	
	}
}
